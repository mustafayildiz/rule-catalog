package com.project.rulecatalog.model;

import com.project.rulecatalog.rule.Rule;
import lombok.Data;

import java.util.List;

@Data
public class RuleWrap {
    private Rule rule;
    private String ruleCatalog;
    private List<String> steps;
}

