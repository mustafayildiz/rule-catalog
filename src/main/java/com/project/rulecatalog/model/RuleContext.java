package com.project.rulecatalog.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class RuleContext {

    private final List<String> reasons;
    private HashMap<String, List<Object>> parameters;

    public RuleContext(HashMap<String, List<Object>> parameters) {
        this.reasons = new ArrayList<>();
        this.parameters = parameters;
    }

    public HashMap<String, List<Object>> getParameters() {
        return parameters;
    }

    public void addFailureReason(String reason) {
        if (Objects.nonNull(reason) && reason.length() > 0)
            this.reasons.add(reason);
    }

    public List<String> getFailureReasons() {
        return reasons;
    }
}

