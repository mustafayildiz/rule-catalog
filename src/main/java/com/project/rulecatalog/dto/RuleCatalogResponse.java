package com.project.rulecatalog.dto;

import lombok.Data;

import java.util.List;

@Data
public class RuleCatalogResponse {
    private List<String> failureReasons;
}
