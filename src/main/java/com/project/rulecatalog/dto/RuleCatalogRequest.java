package com.project.rulecatalog.dto;

import lombok.Data;

import java.util.HashMap;
import java.util.List;

@Data
public class RuleCatalogRequest {
    private String ruleCatalog;
    private String stepId;
    private List<String> stepIds;
    private HashMap<String, List<Object>> parameters;
}
