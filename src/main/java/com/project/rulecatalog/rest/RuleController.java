package com.project.rulecatalog.rest;

import com.project.rulecatalog.dto.RuleCatalogRequest;
import com.project.rulecatalog.dto.RuleCatalogResponse;
import com.project.rulecatalog.service.RuleService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Objects;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/rulecatalog")
public class RuleController {

    @NonNull
    private RuleService ruleService;

    @PostMapping(
            value = "/check",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public RuleCatalogResponse check(@RequestBody RuleCatalogRequest request) {
        if (Objects.isNull(request.getRuleCatalog()) || request.getRuleCatalog().trim().isEmpty()
                || ((Objects.isNull(request.getStepId()) || request.getStepId().trim().isEmpty()) && (Objects.isNull(request.getStepIds()) || request.getStepIds().isEmpty()))) {
            RuleCatalogResponse response = new RuleCatalogResponse();
            response.setFailureReasons(Collections.singletonList("ruleCatalog and stepId must not be empty!!!"));
            return response;
        }
        return ruleService.check(request);
    }
}