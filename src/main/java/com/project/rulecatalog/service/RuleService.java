package com.project.rulecatalog.service;

import com.project.rulecatalog.dto.RuleCatalogRequest;
import com.project.rulecatalog.dto.RuleCatalogResponse;
import com.project.rulecatalog.model.RuleContext;
import com.project.rulecatalog.model.RuleWrap;
import com.project.rulecatalog.model.annotation.RuleCatalog;
import com.project.rulecatalog.model.annotation.RuleStep;
import com.project.rulecatalog.rule.Rule;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class RuleService {

    @NonNull
    private List<Rule> rules;
    private List<RuleWrap> ruleWraps;

    @PostConstruct
    private void initRuleWraps() {
        ruleWraps = new ArrayList<>();
        if (Objects.isNull(rules)) return;
        for (Rule rule : rules) {
            RuleWrap ruleWrap = new RuleWrap();
            ruleWrap.setRule(rule);

            for (Annotation annotation : rule.getClass().getAnnotations()) {
                Class<? extends Annotation> type = annotation.annotationType();

                if (type.getTypeName().equals(RuleCatalog.class.getTypeName())) {
                    Method method = Arrays.stream(type.getDeclaredMethods())
                            .filter(x -> x.getName().equals("value"))
                            .findFirst().orElse(null);
                    if (Objects.nonNull(method)) {
                        try {
                            ruleWrap.setRuleCatalog((String) method.invoke(annotation, (Object[]) null));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }

                if (type.getTypeName().equals(RuleStep.class.getTypeName())) {
                    Method method = Arrays.stream(type.getDeclaredMethods())
                            .filter(x -> x.getName().equals("value"))
                            .findFirst().orElse(null);
                    if (Objects.nonNull(method)) {
                        try {
                            ruleWrap.setSteps(Arrays.asList((String[]) method.invoke(annotation, (Object[]) null)));
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            ruleWraps.add(ruleWrap);
        }
    }

    public RuleCatalogResponse check(RuleCatalogRequest request) {
        RuleCatalogResponse result = new RuleCatalogResponse();
        try {
            result.setFailureReasons(new ArrayList<>());

            List<RuleWrap> executibleRules = ruleWraps.stream().filter(wrap -> Objects.equals(wrap.getRuleCatalog(), request.getRuleCatalog())
                    && (matchStep(request.getStepId(), wrap.getSteps()))).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(executibleRules))
                return result;

            RuleContext ruleContext = new RuleContext(request.getParameters());
            executibleRules.forEach(ruleWrap -> ruleWrap.getRule().execute(ruleContext));
            result.getFailureReasons().addAll(ruleContext.getFailureReasons());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            String exMessage = e.getMessage();
            if (Objects.isNull(exMessage)) exMessage = "Sistemsel bir hata meydana geldi";
            result.setFailureReasons(Collections.singletonList(exMessage));
        }
        return result;
    }

    private boolean matchStep(String stepId, List<String> steps) {
        if (Objects.isNull(stepId) || stepId.isEmpty())
            return false;
        if (Objects.isNull(steps) || steps.isEmpty()||steps.stream().anyMatch(step -> Objects.equals(step, stepId)))
            return true;

        if (steps.size() == 1 && steps.stream().anyMatch(step -> step.indexOf("*") > 0)) {
            String step = steps.get(0);
            int starcharindex = step.indexOf("*");
            String stepWithoutStar = step.substring(0, starcharindex);
            if (stepId.startsWith(stepWithoutStar))
                return true;
        }
        return false;
    }
}
