package com.project.rulecatalog.rule;

import com.project.rulecatalog.model.RuleContext;
import com.project.rulecatalog.model.annotation.RuleCatalog;
import com.project.rulecatalog.model.annotation.RuleStep;
import org.springframework.stereotype.Component;

@Component
@RuleCatalog("Merhaba")
@RuleStep("Dünya")
public class Dummy implements Rule{
    @Override
    public boolean execute(RuleContext ruleContext) {
        ruleContext.addFailureReason("Çok Hata var");
        return false;
    }
}
