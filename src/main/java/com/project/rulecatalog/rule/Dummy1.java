package com.project.rulecatalog.rule;

import com.project.rulecatalog.model.RuleContext;
import com.project.rulecatalog.model.annotation.RuleCatalog;
import org.springframework.stereotype.Component;

@Component
@RuleCatalog("Dummy-Catalog")
public class Dummy1 implements Rule{
    @Override
    public boolean execute(RuleContext ruleContext) {
        ruleContext.addFailureReason("Bu işte bir hata var");
        return false;
    }
}
