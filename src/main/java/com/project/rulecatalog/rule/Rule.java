package com.project.rulecatalog.rule;

import com.project.rulecatalog.model.RuleContext;


public interface Rule {
    boolean execute(RuleContext ruleContext);
}

